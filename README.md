TakeHome Assignment
===================

This is a Android app which communicates with the public Github API in order to display information about a specific user.

## Summary
The app accepts a github user's id as input and display the specified user's avatar and name. 

For each public repository owned by the user, the name and description are shown in a scrollable list. 

When a repository is selected, a card pops up with information about when it was last updated, how many stars it has, and how many times it has been forked. 

## Building the Sample App

First, clone the repo:

'git clone https://bitbucket.org/gaddiprem/takehome.git'


Building the sample depends on your build tools.

### Android Studio (Recommended)

(These instructions were tested with Android Studio version 3.5)

* Open Android Studio and select `File->Open...` or from the Android Launcher select `Import project (Eclipse ADT, Gradle, etc.)` and navigate to the root directory of your project.
* Select the directory or drill in and select the file `build.gradle` in the cloned repo.
* Click 'OK' to open the the project in Android Studio.
* A Gradle sync should start, but you can force a sync and build the 'app' module as needed.

### Gradle (command line)

* Build the APK: `./gradlew build`

## Running the Sample App

Connect an Android device to your development machine.

### Android Studio

* Select `Run -> Run 'app'` (or `Debug 'app'`) from the menu bar
* Select the device you wish to run the app on and click 'OK'
