package com.one.info.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.one.info.viewmodel.SearchVM;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BindingAdapterUtils {


    @BindingAdapter("imageLoadedVisibility")
    public static void checkImageLoaded(LinearLayout layout, Drawable drawable){
        if(drawable!=null){
            layout.setVisibility(View.VISIBLE);
            layout.startLayoutAnimation();
        }else {
            layout.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("onClick")
    public static void onClickSearch(View view, SearchVM viewmodel){
        view.setOnClickListener(v -> {
            Activity activity=(Activity) v.getContext();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            viewmodel.onClickSearchButton();
        });
    }

    @BindingAdapter("formatText")
    public static void setText(TextView view, Date date) {
        if(date!=null) {
            SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy h:mm:ss aa", Locale.US);
            String formatted = format.format(date);
            view.setText(formatted);
        }
    }

    @BindingAdapter("startAnimation")
    public static void startAnimation(View view,Integer loaded){

        if(loaded!=null && loaded.equals(View.GONE)) {
            ((RecyclerView) view).startLayoutAnimation();
        }

    }

    /*@BindingAdapter("visibilityConverterInverse")
    public static void getVisibilityInverse(View view, Integer status){
        if(status!=null) {
            view.setVisibility(status.equals(View.VISIBLE) ? View.GONE : View.VISIBLE);
        }
    }*/
}
