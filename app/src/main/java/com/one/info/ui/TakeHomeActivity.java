package com.one.info.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.one.info.model.ViewStatus;
import com.one.info.model.Repo;
import com.one.info.model.User;
import com.one.info.service.GithubApi;
import com.one.info.service.GithubRestService;
import com.one.info.ui.databinding.ActivityTakehomeBinding;
import com.one.info.ui.databinding.RepoItemDetailPopupBinding;
import com.one.info.viewmodel.ReposVM;
import com.one.info.viewmodel.SearchVM;

import java.util.Collections;

public class TakeHomeActivity extends AppCompatActivity {

    private static final String TAG = TakeHomeActivity.class.getName();

    private ReposVM reposViewModel;
    private SearchVM searchViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GithubApi githubApi=GithubRestService.getInstance().getGithubApi();
        reposViewModel = ViewModelProviders.of(this).get(ReposVM.class);
        reposViewModel.init(githubApi);
        searchViewModel=ViewModelProviders.of(this).get(SearchVM.class);
        searchViewModel.init(githubApi);

        ActivityTakehomeBinding activityTakehomeBinding=DataBindingUtil.setContentView(this,R.layout.activity_takehome);
        activityTakehomeBinding.setSearchViewModel(searchViewModel);
        activityTakehomeBinding.setReposViewModel(reposViewModel);
        activityTakehomeBinding.setLifecycleOwner(this);// Required to update UI with LiveData

        setupSearchVMListeners();
        setupReposVMClickListeners();

    }

    public void setupSearchVMListeners(){
        searchViewModel.viewStatus.observe(this,viewStatus -> {

            if(viewStatus.getState().equals(ViewStatus.State.SUCCESS)){
                loadImage(searchViewModel.user.getValue());
            }
        });

        searchViewModel.searchEmptyFieldError.observe(this, new Observer<String>() {// called  on click search is
            @Override
            public void onChanged(String error) {
                if(error.equals("")){
                    reposViewModel.setRepos(Collections.<Repo>emptyList());
                }
            }
        });

    }

    public void loadImage(final User user){

        Glide.with(this)
                .load(user.getAvatar_url())
                .apply(new RequestOptions().override(300, 300))
                .override(300,300)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(Glide.with(this).load(user.getAvatar_url())) //on  errror retry
                .into(new CustomTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        searchViewModel.image.setValue(resource);
                        reposViewModel.loadRepos(searchViewModel.searchText.getValue().trim());
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        searchViewModel.image.setValue(null);
                        searchViewModel.viewStatus.setValue(new ViewStatus("Image failed to load", View.VISIBLE, ViewStatus.State.FAILED));
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        searchViewModel.image.setValue(placeholder);
                    }
                });
                /*Picasso.get()
                        .load(user.getAvatar_url())
                        .resize(300, 300).into(new BindableFieldTarget(searchViewModel.imageUrl, getResources()));*/
    }

    private void setupReposVMClickListeners() {
        /*reposViewModel.viewStatus.observe(this,viewStatus -> {
            switch (viewStatus.getState()){
                case LOADING:

                case SUCCESS:
                case FAILED:
            }
            if(viewStatus.getState().equals(ViewStatus.State.SUCCESS))
        });*/
        reposViewModel.selected.observe(this, new Observer<Repo>() {
            @Override
            public void onChanged(Repo repo) {
                if (repo != null) {
                    RepoItemDetailPopupBinding binding= DataBindingUtil.inflate(LayoutInflater.from(TakeHomeActivity.this),R.layout.repo_item_detail_popup,null,false);
                    binding.setRepo(repo);

                    BottomSheetDialog dialog = new BottomSheetDialog(TakeHomeActivity.this);
                    dialog.setContentView(binding.getRoot());
                    dialog.show();
                }
            }
        });

    }

    @UiThread
    public void showToast(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG);
    }

    /*@UiThread
    public void showSnackBar(String message){
        Snackbar.make(recyclerView, message, Snackbar.LENGTH_LONG).show();
    }*/

}
