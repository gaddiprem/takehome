package com.one.info.viewmodel;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.one.info.adapters.ReposAdapter;
import com.one.info.model.Repo;
import com.one.info.model.ViewStatus;
import com.one.info.service.GithubApi;
import com.one.info.ui.R;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ReposVM extends ViewModel {

    private static final String TAG = ReposVM.class.getName();

    public MutableLiveData<ViewStatus> viewStatus;
    public MutableLiveData<Repo> selected;
    public ReposAdapter adapter;

    private CompositeDisposable disposable;

    private GithubApi githubApi;

    public void init(GithubApi githubApi) {
        disposable=new CompositeDisposable();
        this.githubApi = githubApi;
        adapter = new ReposAdapter(R.layout.repo_item, this);
        viewStatus=new MutableLiveData<>();
        selected=new MutableLiveData<>();

    }

    public void setRepos(List<Repo> repos){
        this.adapter.setRepos(repos);
//        this.adapter.notifyDataSetChanged();
    }

    public void onItemClick(Integer index) {
        Repo repo = getRepoAt(index);
        selected.setValue(repo);
    }

    public Repo  getRepoAt(Integer index){
        return this.adapter.getRepos().get(index);
    }

    public void loadRepos(String username){
        disposable.add(githubApi.getRepos(username)
                  .subscribeOn(Schedulers.io())
                  .observeOn(AndroidSchedulers.mainThread())
                  .doOnSubscribe(disposable1 -> onLoading())
                  .subscribe(this::onSuccess,this::onError));
    }

    private void onLoading() {
        Log.d(TAG,"Loading.....");
        viewStatus.setValue(new ViewStatus("Loading.....",View.VISIBLE, ViewStatus.State.LOADING));
    }

    private void onSuccess(List<Repo> repos) {
        Log.d(TAG,"Repos["+repos.size()+"] loaded...");
        this.adapter.setRepos(repos);
        if(repos.isEmpty()){
            viewStatus.setValue(new ViewStatus("No repos found",View.VISIBLE, ViewStatus.State.SUCCESS));
        }else {
            viewStatus.setValue(new ViewStatus("", View.GONE, ViewStatus.State.SUCCESS));
        }
    }

    private void onError(Throwable error) {
        Log.e(TAG,error.getLocalizedMessage());
        viewStatus.setValue(new ViewStatus(error.getLocalizedMessage(),View.VISIBLE, ViewStatus.State.FAILED));
    }



    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }


}
