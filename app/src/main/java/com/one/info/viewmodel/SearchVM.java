package com.one.info.viewmodel;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.one.info.model.User;
import com.one.info.model.ViewStatus;
import com.one.info.service.GithubApi;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SearchVM   extends ViewModel {

    private static final String TAG = SearchVM.class.getName();

    private CompositeDisposable disposable;
    public MutableLiveData<String> searchText;
    public MutableLiveData<User> user;
    public MutableLiveData<Drawable>  image;
    public MutableLiveData<ViewStatus> viewStatus;
    public MutableLiveData<String> searchEmptyFieldError;

    private GithubApi githubApi;

    public void init(GithubApi githubApi){
        disposable=new CompositeDisposable();
        this.githubApi = githubApi;
        user=new MutableLiveData<>();
        searchText=new MutableLiveData<>();
        viewStatus=new MutableLiveData<>();
        searchEmptyFieldError=new MutableLiveData<>();
        image=new MutableLiveData<>();

    }

    public void onClickSearchButton(){
        if(searchText.getValue()!=null && !searchText.getValue().isEmpty()){
            image.setValue(null);
            searchEmptyFieldError.setValue("");
            loadUser();
        }else {
            searchEmptyFieldError.setValue("User Id cannot be empty");
        }
    }

    public void loadUser(){
        disposable.add(githubApi.getUser(searchText.getValue())
                  .subscribeOn(Schedulers.io())
                  .observeOn(AndroidSchedulers.mainThread())
                  .doOnSubscribe(disposable1 -> onLoading())
                  .subscribe(this::onSuccess,this::onError));
    }

    private void onLoading() {
        Log.d(TAG,"Searching.....");
        viewStatus.setValue(new ViewStatus("Searching.....",View.VISIBLE, ViewStatus.State.LOADING));
    }

    private void onSuccess(User user) {
        Log.d(TAG,"Loaded User"+ user.getName());
        this.user.setValue(user);
        viewStatus.setValue(new ViewStatus("",View.GONE, ViewStatus.State.SUCCESS));
    }

    private void onError(Throwable error) {
        Log.e(TAG,error.getLocalizedMessage());
        viewStatus.setValue(new ViewStatus(error.getLocalizedMessage(),View.VISIBLE, ViewStatus.State.FAILED));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }

}
