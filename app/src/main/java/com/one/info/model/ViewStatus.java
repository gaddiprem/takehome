package com.one.info.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViewStatus {

    private String message;
    private Integer visible;
    private State state;

    public enum State{
        LOADING(0), SUCCESS(1),FAILED(-1);
        public int value;
        State(int val) {
            value = val;
        }
    }
}
