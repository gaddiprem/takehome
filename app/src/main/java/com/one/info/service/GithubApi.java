package com.one.info.service;

import com.one.info.model.Repo;
import com.one.info.model.User;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GithubApi {

    public static final String BASE_URL = "https://api.github.com/users/";

    @GET("{userId}")
    Observable<User> getUser(@Path("userId") String userId);

    @GET("{userId}/repos")
    Observable<List<Repo>> getRepos(@Path("userId") String userId);


}
