package com.one.info.service;

import lombok.Getter;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static com.one.info.service.GithubApi.BASE_URL;


public class GithubRestService  {

    @Getter private GithubApi githubApi;

    private static GithubRestService githubRestService;

    public static GithubRestService getInstance(){
        if (githubRestService == null){
            githubRestService = new GithubRestService();
        }
        return githubRestService;
    }


    private GithubRestService(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        this.githubApi=retrofit.create(GithubApi.class);
    }

}
