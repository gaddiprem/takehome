package com.one.info.service;

import com.one.info.model.User;
import com.one.info.service.GithubApi;
import com.one.info.service.GithubRestService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GithubRestServiceGetUserTest {


    @Mock
    private GithubApi githubApi;
    private String userId;
    private User  sampleUser;

    @Before
    public void setUp()  {
        MockitoAnnotations.initMocks(this);
//        githubRestService= Mockito.mock(GithubRestService.class);
        this.userId="octocat";
        this.sampleUser=new User("The Octocat","https://avatars3.githubusercontent.com/u/583231?v=4");
    }

    @Test
    public void testGetUser_200OkResponse_InvokesCorrectApiCall() {

        //Given
        when(githubApi.getUser(userId)).thenReturn(Observable.just(sampleUser));

        //when
        TestObserver<User> subscriber=githubApi.getUser(userId).test();
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertNoErrors().assertValue(sampleUser);
    }

    @Test
    public void testGetUser_IOException_TerminatedWithIOException() {

        //Given
        when(githubApi.getUser(userId)).thenReturn(getIOExceptionError());

        //when
        TestObserver<User> subscriber=githubApi.getUser(userId).test();
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertError(IOException.class);
    }

    @Test
    public void testGetUser_OtherHttpError_TerminatedWith403Error() {

        //Given
        when(githubApi.getUser(userId)).thenReturn(get403ForbiddenError());

        //when
        TestObserver<User> subscriber=githubApi.getUser(userId).test();
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertError(HttpException.class);
    }



    private Observable getIOExceptionError() {
        return Observable.error(new IOException());
    }

    private Observable<User> get403ForbiddenError() {
        return Observable.error(new HttpException(Response.error(403, ResponseBody.create("Forbidden",MediaType.parse("application/json")))));
    }

}
