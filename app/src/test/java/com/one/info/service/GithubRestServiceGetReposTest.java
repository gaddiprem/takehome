package com.one.info.service;

import com.one.info.model.Repo;
import com.one.info.service.GithubApi;
import com.one.info.service.GithubRestService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GithubRestServiceGetReposTest {


    @Mock
    private GithubApi githubApi;
    private String userId;

    @Before
    public void setUp()  {
        MockitoAnnotations.initMocks(this);
        this.userId="octocat";
    }

    @Test
    public void testGetRepos_200OkResponse_InvokesCorrectApiCall() {

        //Given
        when(githubApi.getRepos(userId)).thenReturn(Observable.just(getSampleRepos()));

        //when
        TestObserver<List<Repo>> subscriber=githubApi.getRepos(userId).test();
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertNoErrors().assertValue(getSampleRepos());
    }

    @Test
    public void testGetRepos_IOException_TerminatedWithIOException() {

        //Given
        when(githubApi.getRepos(userId)).thenReturn(getIOExceptionError());

        //when
        TestObserver<List<Repo>> subscriber=githubApi.getRepos(userId).test();
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertError(IOException.class);
    }

    @Test
    public void testGetRepos_OtherHttpError_TerminatedWith403Error() {

        //Given
        when(githubApi.getRepos(userId)).thenReturn(get403ForbiddenError());

        //when
        TestObserver<List<Repo>> subscriber=githubApi.getRepos(userId).test();
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertError(HttpException.class);
    }


    private List<Repo> getSampleRepos(){
        List<Repo> repos=new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Repo repo1 = new Repo("boysenberry-repo-1", "Testing", sdf.parse("2019-09-12T16:21:29Z"),2,4);
            Repo repo2 = new Repo("git-consortium", "This repo is for demonstration purposes only.", sdf.parse("2019-03-26T02:39:00Z"),9,34);
            Repo repo3 = new Repo("hello-worId", "My first repository on GitHub.", sdf.parse("2019-09-30T17:49:57Z"),18,49);
            Repo repo4 = new Repo("Hello-World", "My first repository on GitHub!", sdf.parse("2019-10-11T01:30:30Z"),1504,1367);
            Repo repo5 = new Repo("linguist", "Language Savant. If your repository's language is being reported incorrectly, send us a pull request!", sdf.parse("2019-09-14T17:21:08Z"),20,64);
            repos.add(repo1);
            repos.add(repo2);
            repos.add(repo3);
            repos.add(repo4);
            repos.add(repo5);
        }catch (Exception e){
            e.printStackTrace();
        }
        return repos;

    }

    private Observable getIOExceptionError() {
        return Observable.error(new IOException());
    }

    private Observable<List<Repo>> get403ForbiddenError() {
        return Observable.error(new HttpException(Response.error(403, ResponseBody.create("Forbidden",MediaType.parse("application/json")))));

    }

}
