package com.one.info.viewmodel;

import android.view.View;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.one.info.model.User;
import com.one.info.model.ViewStatus;
import com.one.info.service.GithubApi;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class SearchVMTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private SearchVM viewModel;
    private String userId;
    private User  sampleUser;

    @Mock
    private Observer<ViewStatus> viewStatusObserver;

    @Mock
    private Observer<User> userObserver;

    @Mock
    private GithubApi githubApi;

    @BeforeClass
    public static void setupClass() {
        // Override the default AndroidSchedulers.mainThread() Scheduler
        // This is necessary here because if the static initialization block in AndroidSchedulers is executed before this then the Android SDK dependent version will be provided instead.
        //
        // This would cause a java.lang.ExceptionInInitializerError when running the test as a
        // Java JUnit test as any attempt to resolve the default underlying implementation of the AndroidSchedulers.mainThread() will fail as it relies on unavailable Android dependencies.
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
    }

    @Before
    public void setUp()  {
        this.userId="octocat";
        this.sampleUser=new User("The Octocat","https://avatars3.githubusercontent.com/u/583231?v=4");

        MockitoAnnotations.initMocks(this);

        viewModel=new SearchVM();
        viewModel.init(githubApi);
        viewModel.searchText.postValue(userId);
        viewModel.viewStatus.observeForever(viewStatusObserver);
        viewModel.user.observeForever(userObserver);
    }

    @Test
    public void testNull() {
        assertNotNull(viewModel.viewStatus);
        assertTrue(viewModel.viewStatus.hasObservers());
    }


    @Test
    public void testGetUser_onLoading() {

        when(githubApi.getUser(userId)).thenReturn(Observable.just(sampleUser));
        viewModel.loadUser();

        verify(viewStatusObserver).onChanged(new ViewStatus("Searching.....",View.VISIBLE, ViewStatus.State.LOADING));
    }


    @Test
    public void testGetUser_onSuccess() {

        when(githubApi.getUser(userId)).thenReturn(Observable.just(sampleUser));
        viewModel.loadUser();

        verify(viewStatusObserver).onChanged(new ViewStatus("", View.GONE, ViewStatus.State.SUCCESS));
        verify(userObserver).onChanged(sampleUser);
    }

    @Test
    public void testGetUser_onError() {

        Exception sampleException=new Exception("error");
        when(githubApi.getUser(userId)).thenReturn(Observable.error(sampleException));
        viewModel.loadUser();

        verify(viewStatusObserver).onChanged(new ViewStatus(sampleException.getLocalizedMessage(),View.VISIBLE, ViewStatus.State.FAILED));
    }

    @AfterClass
    public static void tearDownClass() {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }

}
