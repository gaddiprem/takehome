package com.one.info.viewmodel;

import android.view.View;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.one.info.model.Repo;
import com.one.info.model.ViewStatus;
import com.one.info.service.GithubApi;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ReposVMTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    private ReposVM viewModel;
    private String userId;

    @Mock
    private Observer<ViewStatus> viewStatusObserver;

    @Mock
    private Observer<Repo> selectedRepo;

    @Mock
    private GithubApi githubApi;

    @BeforeClass
    public static void setupClass() {
        // Override the default AndroidSchedulers.mainThread() Scheduler
        // This is necessary here because if the static initialization block in AndroidSchedulers is executed before this then the Android SDK dependent version will be provided instead.
        //
        // This would cause a java.lang.ExceptionInInitializerError when running the test as a
        // Java JUnit test as any attempt to resolve the default underlying implementation of the AndroidSchedulers.mainThread() will fail as it relies on unavailable Android dependencies.
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
    }

    @Before
    public void setUp() {
        this.userId="octocat";
        MockitoAnnotations.initMocks(this);

        viewModel=new ReposVM();
        viewModel.init(githubApi);
        viewModel.viewStatus.observeForever(viewStatusObserver);
        viewModel.selected.observeForever(selectedRepo);
    }




    @Test
    public void testNull() {
        assertNotNull(viewModel.viewStatus);
        assertTrue(viewModel.viewStatus.hasObservers());
    }

    @Test
    public void test_onClickSearchButton_ReturnCorrectObject() {
        List<Repo> repos = getSampleRepos();
        // When
        viewModel.adapter.setRepos(repos);
        viewModel.onItemClick(0);
        // then
        verify(selectedRepo).onChanged(repos.get(0));
    }

    @Test
    public void testGetRepos_onLoading() {

        when(githubApi.getRepos(userId)).thenReturn(Observable.just(getSampleRepos()));
        viewModel.loadRepos(userId);

        verify(viewStatusObserver).onChanged(new ViewStatus("Loading.....",View.VISIBLE, ViewStatus.State.LOADING));
    }


    @Test
    public void testGetRepos_onSuccess_ValidRepos() {

        when(githubApi.getRepos(userId)).thenReturn(Observable.just(getSampleRepos()));
        viewModel.loadRepos(userId);

        verify(viewStatusObserver).onChanged(new ViewStatus("",View.GONE, ViewStatus.State.SUCCESS));

    }

    @Test
    public void testGetRepos_onSuccess_EmptyRepos() {

        when(githubApi.getRepos(userId)).thenReturn(Observable.just(Collections.emptyList()));
        viewModel.loadRepos(userId);

        verify(viewStatusObserver).onChanged(new ViewStatus("No repos found",View.VISIBLE, ViewStatus.State.SUCCESS));

    }

    @Test
    public void testGetRepos_onError() {
        Exception sampleException=new Exception("error");
        when(githubApi.getRepos(userId)).thenReturn(Observable.error(sampleException));
        viewModel.loadRepos(userId);

        verify(viewStatusObserver).onChanged(new ViewStatus(sampleException.getLocalizedMessage(),View.VISIBLE, ViewStatus.State.FAILED));
    }


    public List<Repo> getSampleRepos(){

            List<Repo> repos=null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            try {
                Repo repo1 = new Repo("boysenberry-repo-1", "Testing", sdf.parse("2019-09-12T16:21:29Z"),2,4);
                Repo repo2 = new Repo("git-consortium", "This repo is for demonstration purposes only.", sdf.parse("2019-03-26T02:39:00Z"),9,34);
                Repo repo3 = new Repo("hello-worId", "My first repository on GitHub.", sdf.parse("2019-09-30T17:49:57Z"),18,49);
                Repo repo4 = new Repo("Hello-World", "My first repository on GitHub!", sdf.parse("2019-10-11T01:30:30Z"),1504,1367);
                Repo repo5 = new Repo("linguist", "Language Savant. If your repository's language is being reported incorrectly, send us a pull request!", sdf.parse("2019-09-14T17:21:08Z"),20,64);

                repos= Arrays.asList(repo1,repo2,repo3,repo4,repo5);

            }catch (Exception e){
                e.printStackTrace();
            }
            return repos;
    }


    @AfterClass
    public static void tearDownClass() {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }

}
