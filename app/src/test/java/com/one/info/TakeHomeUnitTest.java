/*
package com.one.info;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.github.leonardoxh.livedatacalladapter.Resource;
import com.one.info.model.Repo;
import com.one.info.model.User;
import com.one.info.repository.GithubRepository;
import com.one.info.viewmodel.ReposVM;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class TakeHomeUnitTest {

    // chose a Character random from this String
    String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "0123456789"
            + "abcdefghijklmnopqrstuvxyz";

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private GithubRepository githubRepository;

    @Mock
    private Observer<Repo> observer;

    private ReposVM viewModel;

    @Before
    public void initMocks() {
        viewModel=mock(ReposVM.class);
        when(viewModel.g()).thenReturn(userData);
        MockitoAnnotations.initMocks(this);
        githubRepository=new GithubRepository();
        viewModel=new ReposVM();
        viewModel.init();

        viewModel.getSelected().observeForever(observer);
    }

    @Test
    public void test_GetUser(){
        String userame="octocat";
        User user=new User("test","testinng");
        viewModel.setUser(user);

        Mockito.verify(viewModel.user).set(user);
        Resource<User> resource=githubRepository.getGithubApi().loadUser(userame).getValue();
        if(resource.isSuccess()){
            Assert.assertEquals(resource.getResource().getName(),userame);
        }else {
            Assert.fail();
        }
    }

    @Test
    public void test_clickReturnCorrectObject() {
        List<Repo> repos=generateRepos();

        viewModel.setRepos(repos);

        // When  cliick on Repo item \
        viewModel.onItemClick(0);

        // then check  for correctness
        verify(observer).onChanged(repos.get(0));
    }

    private List<Repo> generateRepos() {
        List<Repo> repos = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Repo repo = new Repo();
            repo.setName("Repo " + i);
            repo.setDescription(getAlphaNumericString(100));
            repo.setForks(new Random().nextInt());
            repo.setStargazers_count(new Random().nextInt());
            repo.setUpdated_at(Calendar.getInstance().getTime());
            repos.add(repo);
        }
        return repos;
    }

    public String getAlphaNumericString(int n) {
        // lower limit for LowerCase Letters
        int lowerLimit = 97;

        // lower limit for LowerCase Letters
        int upperLimit = 122;

        Random random = new Random();

        // Create a StringBuffer to store the result
        StringBuffer r = new StringBuffer(n);

        for (int i = 0; i < n; i++) {
            // take a random value between 97 and 122
            int nextRandomChar = lowerLimit + (int)(random.nextFloat() * (upperLimit - lowerLimit + 1));

            // append a character at the end of bs
            r.append((char)nextRandomChar);
        }

        // return the resultant string
        return r.toString();
    }
}
*/
